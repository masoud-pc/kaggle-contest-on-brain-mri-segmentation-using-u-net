# Kaggle contest on Brain MRI Segmentation using U-Net

This is a code to kaggle competition on brain mri segmentation using U-Net model. 

The Dataset is available on 
https://www.kaggle.com/mateuszbuda/lgg-mri-segmentation


![Out](/image/out1.png)
![Out](/image/out2.png)